# CRA Lambda App Hooks

## Description
This uses hooks and is based on the [Create React App Lambda Template](https://github.com/netlify/create-react-app-lambda "link to create react app lambda") by the guys and gals at Netlify. I could have forked the project, but some old habits die hard.

### Processing and/or completion date(s)
April 14, 2020

## How its different

To use the POST method and communicate successfully with the corresponding Lambda endpoint, in terms of local development I needed to add a new file at **./src/setUpProxy.js** file like so:

```sh
const proxy = require('http-proxy-middleware');
module.exports = ( app ) => {
  
  app.use(
    proxy( 
      '/.netlify/functions/' 
      , {
        target: 'http://localhost:9000/' 
        , "pathRewrite": { "^/\\.netlify/functions": "" }
      } 
    )
  );

};

```

You can read up more on the subject of proxying at the [reactjs website](https://create-react-app.dev/docs/proxying-api-requests-in-development/ "link to proxy documentation").


## Please visit
You can see [the lesson at netlify](https://cra-lambda-app-hooks.netlify.app "the lesson at netlify") and decide if this is something for you to play around with at a later date.


## God bless!