import React from "react";

class LambdaDemo extends React.Component{
  
  constructor(){
    super();
    this.state = { loading: false, msg: "" };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick( e ){ 
    // console.log( "e.target.dataset.schmoo", e.target.dataset.schmoo );
    // console.log( "e.target.getAttribute('data-schmoo')" , e.target.getAttribute('data-schmoo') );

    let baseEarl = "/.netlify/functions";
    this.setState( { loading: true } );

    fetch( `${baseEarl}/${e.target.dataset.schmoo}` ).then( response => response.json() )
    .then( data => {
      console.log( "data: " , data );
      this.setState( { loading: false, msg: data.msg } );
    } )
    .catch( err => { console.log( "err: ", err ); } );

  }

  render(){
    
    const { loading, msg } = this.state;

    let element = (
      <React.Fragment>
        <p>
          <button onClick={ this.handleClick } data-schmoo="hello">{ loading ? "Loading..." : "Call Hello!" }</button>
          &nbsp;
          <button onClick={ this.handleClick } data-schmoo="async-dadjoke">{ loading ? "Loading..." : "Call Dad Joke!"}</button>
          <br />
          <span>{ msg }</span>
        </p>
      </React.Fragment>
    );

    return element;
  }
}

export { LambdaDemo };