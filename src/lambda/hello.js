exports.handler = async( event, context ) => {
  // accessible through...
  // http://localhost:9000/.netlify/functions/hello
  
  // context.callbackWaitsForEmptyEventLoop = false;

  // your server-side functionality 
  // console.log( "context", context ); // { clientContext: {} }
  // console.log( "callback", callback );
  console.log( "event.path", event.path ); // "Path parameter", e.g. -- /createTodo
  // console.log( "event.httpMethod", event.httpMethod ); //"Incoming request's method name"
  // // console.log( "event.headers", event.headers ); // {Incoming request headers}
  // console.log( "event.queryStringParameters", event.queryStringParameters ); // {query string parameters }
  // console.log( "event.body", event.body ); // "A JSON string of the request payload."
  // console.log( "event.isBase64Encoded", event.isBase64Encoded ); // "A boolean flag to indicate if the applicable request payload is Base64-encode"

  try{
    const message = 'Konichiwa, cry-babies!';
    const netlifyresponseobject = {
      statusCode: 200
      , body: JSON.stringify( { msg: await message } )
    };
    return netlifyresponseobject;
  }
  catch( err ){
    console.log( 'err' , err );
    const netlifyresponseerror = {};
    return netlifyresponseerror;
  }

};