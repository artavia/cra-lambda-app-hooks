const proxy = require('http-proxy-middleware');
module.exports = ( app ) => {
  
  app.use(
    proxy( 
      '/.netlify/functions/' 
      , {
        target: 'http://localhost:9000/' 
        , "pathRewrite": { "^/\\.netlify/functions": "" }
      } 
    )
  );

};

/* const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api',
    proxy({
      target: 'http://localhost:5000',
      changeOrigin: true,
    })
  );
}; */